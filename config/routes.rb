Rails.application.routes.draw do
  post 'authenticate', to: 'authentication#authenticate'

  resources :trips do
    resources :ratings, only: [:index]
    resources :checklist_items, only: [:index, :create, :destroy, :update]
    resource :evaluation_report, only: [:show, :update]
  end

  resources :users

  resources :schedules, only: [:index] do
    resources :stops, only: [:index]
  end

  resources :feedback_posts, only: [:create]

  resources :population_specs, only: [:create]
  resources :stations, only: [:show]
end
