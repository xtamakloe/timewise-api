class TrainInfo < ApplicationRecord
  belongs_to :trip
  
  after_create :retrieve_feature_list 
  
  def retrieve_feature_list
    # get components to create url http://trains.im/scheduledetail/W88036/2020/09/25/
    train_schedule = self.trip.train_schedule
    
    # ["http:", "", "transportapi.com", "v3", "uk", "train", "service", 
    # "train_uid:W88002", "2020-06-15", "timetable.json?app_id=d7c2a0d3&app_key=196d2a25e8b5b2609d032658f8ae842b"]     
    timetable_url_components = train_schedule.service_timetable.split('/')
    train_uid  = timetable_url_components[7].gsub('train_uid:', '')
    train_time = timetable_url_components[8].gsub('-', '/')
      
    # url => "http://trains.im/scheduledetail/W88036/2020/09/25/"  
    url = "http://trains.im/scheduledetail/#{train_uid}/#{train_time}"  

    # get page html and parse  
    require 'http'
    response = HTTP.get(url)    
    doc = Nokogiri::HTML(response.body.to_s)
    # => features are in the 3rd of 4 spans in the entire page
    feature_list = []
    doc.xpath("//div[contains(@class, 'span4')][2]/ul/li").each {|item| feature_list << item.text}
    self.feature_list = feature_list.join(',')
    save 
  end
end
