class EvaluationReport < ApplicationRecord
  belongs_to :trip
  has_one :activity_data_report, dependent: :destroy 
  has_one :experience_factors_report, dependent: :destroy 
  
  accepts_nested_attributes_for :activity_data_report
  accepts_nested_attributes_for :experience_factors_report
    
  after_create :setup_model
  
  
  def setup_model
    unless self.activity_data_report
      ActivityDataReport.create(evaluation_report: self) 
    end
    unless self.experience_factors_report
      ExperienceFactorsReport.create(evaluation_report: self) 
    end      
  end
  
end
