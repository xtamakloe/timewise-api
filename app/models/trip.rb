class Trip < ApplicationRecord
  belongs_to :user
  belongs_to :train_schedule, dependent: :destroy # dependency added when not sharing train_schedules in v2
  has_many :trip_stations, dependent: :destroy
  has_many :stations, through: :trip_stations
  has_one :train_info, dependent: :destroy
  has_one :checklist, dependent: :destroy
  has_one :evaluation_report, dependent: :destroy
  
  accepts_nested_attributes_for :evaluation_report

  validates :status, presence: true,
            inclusion: {in: %w[upcoming in-progress completed],
                        message: '%{value} is not a valid trip status'}

  # Trip stations and times are created from a TrainSchedule,
  # not by setting attributes directly 
  # So to update, guess you'd need to supply a new TrainSchedule
  # TODO: implement update train times 
  after_create :retrieve_schedule_info, :setup_model



  attr_writer :origin_station_name
  attr_writer :origin_station_code
  attr_writer :destination_station_name
  attr_writer :destination_station_code



  # getters 

  def origin_station_name
    origin_station.try(:name)
  end

  def origin_station_code
    origin_station.try(:code)
  end

  def destination_station_name
    destination_station.try(:name)
  end

  def destination_station_code
    destination_station.try(:code)
  end

  def origin_station
    trip_stations.origin.try(:station)
  end

  def destination_station
    trip_stations.destination.try(:station)
  end

  # def origin
  #   #TODO: change to code only
  #   origin_station.try(:name_with_code)
  # end

  # def destination
  #   #TODO: change to code only
  #   destination_station.try(:name_with_code)
  # end

  def transits
    stations.merge(TripStation.transits)
  end

  # setters 

  def origin_station=(station)
    return if self.trip_stations.where(function: 'origin').present?
    #TODO: check station is not set as destination
require "trip"

    TripStation.create(function: 'origin', trip: self, station_id: station.id)
  end

  def destination_station=(station)
    return if self.trip_stations.where(function: 'destination').present?
    #TODO: check station is not set as origin

    TripStation.create(function: 'destination', trip: self, station_id: station.id)
  end

  def rating_cells
    self.train_schedule.get_rating_cells
  end

  def operator_name
    self.train_schedule.try(:operator_name)
  end
  
  def duration
    time_array = ((self.arrives_at - self.departs_at) / 3600).round(2).to_s.split('.')

    hr  = time_array[0]
    min = (time_array[1].to_f / 10 * 60).round.to_s[0..1]

    "#{"#{hr}h" unless hr == '0'} #{min}m".squish
  end


  def feature_list
    self.train_info.feature_list.to_s # disabled due to v3 endpoint
  end


  def covid_url
    Operator.where(
      code: self
              .try(:train_schedule)
              .try(:operator))
      .try(:first)
      .try(:covid_url)
  end


  def info_url
    Operator.where(
      code: self
              .try(:train_schedule)
              .try(:operator))
      .try(:first)
      .try(:info_url)
  end


  def crowding_url
    Operator.where(
      code: self
              .try(:train_schedule)
              .try(:operator))
      .try(:first)
      .try(:crowding_url)
  end


  private


  # Get trip information from train schedule
  def retrieve_schedule_info
    schedule = self.train_schedule

    if schedule
      self.origin_station      = schedule.start_station
      self.destination_station = schedule.end_station

      self.departs_at = schedule.starts_at
      self.arrives_at = schedule.ends_at
      save
    end
  end
  
  def setup_model 
    TrainInfo.create(trip: self) # v3 endpoint doesn't allow this
    Checklist.create(trip: self)
    EvaluationReport.create(trip: self)
  end
end
