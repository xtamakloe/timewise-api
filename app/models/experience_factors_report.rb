class ExperienceFactorsReport < ApplicationRecord
  belongs_to :evaluation_report
  has_many :experience_factors, dependent: :destroy 

  accepts_nested_attributes_for :experience_factors 
end
