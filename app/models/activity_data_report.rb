class ActivityDataReport < ApplicationRecord
  belongs_to :evaluation_report
  has_many :activities, dependent: :destroy
  
  after_save :process_activity_data   
  
  attr_accessor :activity_data
  
  def activity_codes
    self.activities.map(&:code).join(',')    
  end
  
  
  private 
  
  # activity_data => 'communicate,read,audio...'
  def process_activity_data
    if self.activity_data.present?
      self.activity_data.split(',').each do |code|    
        if code.squish.present? && self.activities.where(code: code.squish).empty?
          Activity.create(code: code, activity_data_report: self) 
        end
      end      
    end
  end
  
end
