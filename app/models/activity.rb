class Activity < ApplicationRecord
  belongs_to :activity_data_report
  
  validates :code, presence: true, 
                   uniqueness: { scope: :activity_data_report_id }
end
