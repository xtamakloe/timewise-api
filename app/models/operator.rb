class Operator < ApplicationRecord
  validates :code, presence: true
  validates :name, presence: true
end
