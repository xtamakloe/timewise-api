class ExperienceFactor < ApplicationRecord
  belongs_to :experience_factors_report
  
  validates :code, presence: true, uniqueness: { scope: :experience_factors_report_id }
  validates :rating, presence: true, inclusion: { in: [ 0, 1 ] }
end
