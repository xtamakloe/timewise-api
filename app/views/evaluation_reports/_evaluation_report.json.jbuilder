json.evaluation_report(evaluation_report, 
      :id, 
	  :pre_trip_prep_rating, 
      :post_trip_prep_rating, 
      :trip_experience_rating, 
      :time_use_rating, 
      :trip_notes, 
     )

json.evaluation_report do 
	json.activity_codes evaluation_report.activity_data_report.activity_codes 
	
	json.experience_factors_report do 
		json.id evaluation_report.experience_factors_report.id  
		
		json.experience_factors evaluation_report.experience_factors_report.experience_factors
	end
end