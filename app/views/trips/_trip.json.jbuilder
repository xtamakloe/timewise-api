json.trip(trip,
          :id,
          :origin_station_name,
          :origin_station_code,
          :destination_station_name,
          :destination_station_code,
          :departs_at,
          :arrives_at,
          :duration,
          :trip_type,
          :purpose,
          :travel_direction,
          :status,
          :rating,
          :operator_name,
          :feature_list,
          :info_url,
          :covid_url,
          :crowding_url,
     )




json.trip do 

	#json.checklist_items trip.checklist.checklist_items, partial: 'checklist_items/checklist_item', as: :checklist_items

	json.checklist_items trip.checklist.checklist_items, partial: 'checklist_items/checklist_item', as: :checklist_item
	
	json.partial! 'evaluation_reports/evaluation_report', evaluation_report: trip.evaluation_report
end



#json.rating_cells trip.rating_cells, partial: 'ratings/rating', as: :rating