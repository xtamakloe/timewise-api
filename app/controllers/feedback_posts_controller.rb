class FeedbackPostsController < ApplicationController
  def create
    post = current_user.feedback_posts.new(post_params)

    if post.save
      render json: {}, status: :ok
    else
      render json: {}, status: :unprocessable_entity
    end
  end


  private


  def post_params
    params.require(:feedback_post).permit(
      :message,
      :image,
    )
  end
end
