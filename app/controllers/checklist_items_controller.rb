class ChecklistItemsController < ApplicationController  
  before_action :get_trip_checklist, only: [:index, :create, :destroy, :update]
  
  def index
    @checklist_items = @checklist.checklist_items
  end
  
  def create
    @item = @checklist.checklist_items.new(item_params)

    if @item.save
      render 'checklist_items/show', status: :created
    else
      render json: @item.errors, status: :unprocessable_entity
    end  
  end
  
  def update
    @item = @checklist.checklist_items.find(params[:id])
    
    if @item.update(item_params)
      render 'checklist_items/show'
    else
      render json: @item.errors, status: :unprocessable_entity      
    end
  end
  
  def destroy
    @item = @checklist.checklist_items.find(params[:id])
    render json: {}, status: :ok if @item && @item.destroy
  end
  
  private 
  
  def item_params
    params.require(:checklist_item).permit(
      :text, 
      :complete, 
    )
  end
  
  def get_trip_checklist
    @trip = current_user.trips.find(params[:trip_id])
    @checklist = @trip.checklist if @trip
  end
end
