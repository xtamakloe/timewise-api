class TripsController < ApplicationController
  before_action :set_trip, only: [:show, :update, :destroy]

  # GET /trips
  def index
    @trips = current_user.trips

    # render json: @trips
  end

  # GET /trips/1
  def show
    # render json: @trip
  end

  # POST /trips
  def create
    @trip = current_user.trips.new(trip_params)

    schedule = TrainSchedule.find(trip_params[:train_schedule_id])
    # was commented out cos of v3 endpoint
    schedule.get_rating_cells if schedule

    if @trip.save
      # render json: @trip, status: :created, location: @trip
      render 'trips/show', status: :created
    else
      render json: @trip.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /trips/1
  def update
    if @trip.update(trip_params)
      render 'trips/show' #, status: :created
    else
      render json: @trip.errors, status: :unprocessable_entity
    end
  end

  # DELETE /trips/1
  def destroy
    render json: {}, status: :ok if @trip && @trip.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_trip
    @trip = current_user.trips.find(params[:id])
  end

  def trip_params
    params.require(:trip).permit(
        :origin_station_name,
        :origin_station_code,
        :destination_station_name,
        :destination_station_code,

        # :id,
        :departs_at,
        :arrives_at,

        :trip_type,
        :purpose,
        :status,
        :travel_direction,
        :train_schedule_id,
        :rating,
        
        # evaluation data 
        evaluation_report_attributes: [
          :id, 
          :pre_trip_prep_rating, 
          :post_trip_prep_rating, 
          :trip_experience_rating,
          :time_use_rating, 
          :trip_notes,
          activity_data_report_attributes: [
            :activity_data
          ], 
          experience_factors_report_attributes: [
            :id, 
            experience_factors_attributes: [
              :experience_factors_report_id, 
              :code, 
              :rating, 
            ]
          ] 
        ]
        
    )
  end
end