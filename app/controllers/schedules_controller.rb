class SchedulesController < ApplicationController

  # :start_station => code
  # :end_station => code
  # :date DD/MM/YYYY
  # :time HH:MM
  def index
    start_code = params[:start_station]
    end_code   = params[:end_station]
    date       = Date.parse(params[:date])
    time       = params[:time]
    @schedules = []

    # v2 update:
    # => create new schedules for each request. don't reuse
    # # check if schedules have already been created for given start/end/date
    # # use those if present
    # @schedules = TrainSchedule.where(
    #     start_station_code: start_code,
    #     end_station_code: end_code,
    #     starts_at: date.all_day
    # )
    # return if @schedules.present?

    # create new schedules for start/end/date
    require 'http'
    endpoint =
      "http://transportapi.com/v3/uk/train/station/#{start_code}/#{date}/#{time}/timetable.json"\
        "?app_id=#{ENV['TRANSPORT_API_APP_ID']}"\
        "&app_key=#{ENV['TRANSPORT_API_APP_KEY']}"\
        "&calling_at=#{end_code}"\
        "&station_detail=origin,calling_at"\
        "&train_status=passenger"

    response = HTTP.get(endpoint)
    json     = JSON(response.body)
    json['departures']['all'].each do |data|
      schedule = TrainSchedule.new(
        start_station_code: data['station_detail']['origin']['station_code'],
        end_station_code:   data['station_detail']['calling_at'][0]['station_code'],
        starts_at:          DateTime.parse("#{date} #{data['station_detail']['origin']['aimed_departure_time']}"),
        ends_at:            DateTime.parse("#{date} #{data['station_detail']['calling_at'][0]['aimed_arrival_time']}"),
        operator:           data['operator'],
        operator_name:      data['operator_name'],
        train_uid:          data['train_uid'],
        service:            data['service'],
        service_timetable:  data['service_timetable']['id'],
      )
      @schedules << schedule if schedule.save
    end
  end

end

# {"mode"=>"train",
#  "service"=>"22154000",
#  "train_uid"=>"W88002",
#  "platform"=>"4",
#  "operator"=>"EM",
#  "operator_name"=>"East Midlands Trains",
#  "aimed_departure_time"=>nil,
#  "aimed_arrival_time"=>"07:36",
#  "aimed_pass_time"=>nil,
#  "origin_name"=>"Nottingham",
#  "destination_name"=>"London St Pancras",
#  "source"=>"ATOC",
#  "category"=>"XX",
#  "service_timetable"=>{"id"=>"http://transportapi.com/v3/uk/train/service/train_uid:W88002/2020-06-15/timetable.json?app_id=d7c2a0d3&app_key=196d2a25e8b5b2609d032658f8ae842b"},
#  "station_detail"=>{
#      "origin"=>{"station_name"=>"Nottingham", "platform"=>"6", "station_code"=>"NOT", "tiploc_code"=>"NTNG", "aimed_arrival_time"=>nil, "aimed_departure_time"=>"05:32", "aimed_pass_time"=>nil},
#      "destination"=>{"station_name"=>"London St Pancras", "platform"=>"4", "station_code"=>"STP", "tiploc_code"=>"STPX", "aimed_arrival_time"=>"07:36", "aimed_departure_time"=>nil, "aimed_pass_time"=>nil}}}


# json['arrivals']['all']

# TrainSchedule(start_station_code, end_station_code, starts_at, ends_at)
# operator, operator_name, train_uid, service, service_timetable,
#
# origin[station_code]
# origin[aimed_departure_time]
# destination[station_code]
# destination[aimed_arrival_time]

#

# TrainSchedule(start_station_code, end_station_code, starts_at, ends_at)
# GET /schedules
# :start_station => code
# :end_station => code
# :date YYYY-MM-DD
# :time HH:MM
# 'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyMSwiZXhwIjoxNTkyMjY2MjE3fQ.XJD94ldBRvKoyXNlc0hWwujxHHVimWtnl4sFmfkUPis'

=begin
  # GET /schedules
  # :start_station => code
  # :end_station => code
  # :date YYYY-MM-DD
  # :time HH:MM
  def index
    #    start_time = DateTime.parse("#{params[:date]} #{params[:time]}")
    #    @schedules = TrainSchedule.where(
    #                    start_station: params[:station_station],
    #                    end_station: params[:end_station],
    #                    start_time: start_datetime.all_day
    #                  )

    # create a bunch of travel times and return them
    start_station = Station.find_by_code(params[:start_station])
    end_station = Station.find_by_code(params[:end_station])
    start_time = DateTime.parse("#{params[:date]} #{params[:time]}")

    # create transchedules for the date that gets passed in if nothing is created for that day

    # if not train schedules on requested day
    date_string = start_time.to_s.split('T').first
    @schedules = TrainSchedule.where(starts_at: start_time.all_day).order(starts_at: 'asc')
    return @schedules if @schedules.present?

    # create some schedules
    # before time
    (1..6).each do |diff|
      st = DateTime.parse("#{start_time.to_s.split('T').first} #{(start_time.hour - diff - diff).abs}:#{rand(1..59)}")
      et = DateTime.parse("#{date_string} #{start_time.hour - diff}:#{rand(1..59)}")
      schedule = TrainSchedule.create(
        start_station_code: start_station.code,
        end_station_code: end_station.code,
        starts_at: st,
        ends_at: et,
      )
    end
    # after time
    (1..6).each do |diff|
      st = DateTime.parse("#{date_string} #{start_time.hour + diff}:#{rand(1..59)}")
      et = DateTime.parse("#{start_time.to_s.split('T').first} #{(start_time.hour + diff + diff).abs}:#{rand(1..59)}")
      schedule = TrainSchedule.create(
        start_station_code: start_station.code,
        end_station_code: end_station.code,
        starts_at: st,
        ends_at: et,
      )
    end
    @schedules = TrainSchedule.where(starts_at: start_time.all_day).order(starts_at: 'asc')
  end
=end

=begin
  def index
    start_code = params[:start_station]
    end_code   = params[:end_station]
    date       = Date.parse(params[:date])
    time       = params[:time]
    @schedules = []
    # v2 update:
    # => create new schedules for each request. don't reuse
    # # check if schedules have already been created for given start/end/date
    # # use those if present
    # @schedules = TrainSchedule.where(
    #     start_station_code: start_code,
    #     end_station_code: end_code,
    #     starts_at: date.all_day
    # )
    # return if @schedules.present?

    # create new schedules for start/end/date
    require 'http'
    # endpoint =
    #   "https://transportapi.com/v3/uk/train/station/#{start_code}/live.json"\
    #     "?app_id=#{ENV['TRANSPORT_API_APP_ID']}"\
    #     "&app_key=#{ENV['TRANSPORT_API_APP_KEY']}"\
    # #     "&origin=#{start_code}"\
    # #     "&destination=#{end_code}"\
    # #     "&from_offset=PT01:00:00"\
    # #     "&to_offset=PT06:00:00"\
    #     "&darwin=false"\
    #     "&destination=#{end_code}"\
    #     "&train_status=passenger"

    endpoint =
      "http://transportapi.com/v3/uk/train/station/#{start_code}/#{date}/#{time}/timetable.json"\
        "?app_id=#{ENV['TRANSPORT_API_APP_ID']}"\
        "&app_key=#{ENV['TRANSPORT_API_APP_KEY']}"\
        "&calling_at=#{end_code}"\
        "&train_status=passenger"
    # "&live=true"\
    # "&type=arrival,departure,pass"\
    # "&origin=#{start_code}"\
    # "&destination=#{end_code}"\
    # "&from_offset=-PT12:00:00"\
    # "&to_offset=PT12:00:00"\
    # "&station_detail=origin,destination"

    puts '------'
    puts endpoint
    puts '------'

    response = HTTP.get(endpoint)
    json     = JSON(response.body)
    json['arrivals']['all'].each do |data|
      puts data
      schedule = TrainSchedule.new(
        start_station_code: data['station_detail']['origin']['station_code'],
        end_station_code:   data['station_detail']['destination']['station_code'],

        starts_at:          DateTime.parse("#{date} #{data['station_detail']['origin']['aimed_departure_time']}"),
        ends_at:            DateTime.parse("#{date} #{data['station_detail']['destination']['aimed_arrival_time']}"),

        operator:           data['operator'],
        operator_name:      data['operator_name'],
        train_uid:          data['train_uid'],
        service:            data['service'],
        service_timetable:  data['service_timetable']['id'],
      )
      @schedules << schedule if schedule.save
    end
  end
=end

=begin
def index_v3
  #v3 based on national rail enquiries
  # v3
  # get trs in table id=oft
  # for each tr
  #   get td.results-from
  #       div.dep => departs_at value
  #         => tr1.xpath(".//div[@class='dep']").text
  #
  #       div.from
  #         span.result-station => origin_name value
  #           abbr  => origin_station_code value
  #
  #         tr1.xpath(".//div[@class='from']").text.split()
  #         => ["Nottingham", "[NOT]", "Platform", "5"]
  #
  #   get td.results-to
  #       div.arr => arrive_at value
  #         => tr1.xpath(".//div[@class='arr']").text
  #
  #       div.to
  #         span.result-station => origin_name
  #           abbr  => origin_station_code
  #
  #         tr1.xpath(".//div[@class='to']").text.split()
  #         => ["Leicester", "[LEI]", "Platform", "3"]
  # TODO: handle exceptions
  # start/end/date not supplied
  # date wrong format to parse

  start_code = params[:start_station]
  end_code   = params[:end_station]
  date       = params[:date]
  time       = params[:time]
  @schedules = []

  # v3 update
  # => change endpoint for schedules
  # https://ojp.nationalrail.co.uk/service/timesandfares/NOT/LEI/281020/1300/dep
  dt       = date.split('/')
  pdate    = "#{dt[0]}#{dt[1]}#{dt[2][2...4]}"
  endpoint =
    "https://ojp.nationalrail.co.uk/service/timesandfares"\
        "/#{start_code}"\
        "/#{end_code}"\
        "/#{pdate}"\
        "/#{time.gsub(':', '')}/dep"

  puts endpoint
  require 'http'
  response = HTTP.get(endpoint)
  require 'nokogiri'
  doc   = Nokogiri::HTML(response.body.to_s)
  table = doc.xpath('//*[@id="oft"]')
  puts 'llllllll'
  puts table
  puts 'llllllll'
  trs = table.xpath("//tbody/tr[contains(@class, 'mtx')]")
  trs.each do |tr|
    starts_at = DateTime.parse(tr.xpath(".//div[@class='dep']").text.strip)
    ends_at   = DateTime.parse(tr.xpath(".//div[@class='arr']").text.strip)

    from_data = tr.xpath(".//div[@class='from']").text.split()
    to_data   = tr.xpath(".//div[@class='to']").text.split()

    puts '-----'
    puts starts_at
    puts ends_at
    puts from_data
    puts to_data
    puts '-----'

    schedule = TrainSchedule.new(
      start_station_code: from_data[1].gsub('[', '').gsub(']', ''),
      end_station_code:   to_data[1].gsub('[', '').gsub(']', ''),
      starts_at:          starts_at,
      ends_at:            ends_at,
    # operator: data['operator'],
    # operator_name: data['operator_name'],
    # train_uid: data['train_uid'],
    # service: data['service'],
    # service_timetable: data['service_timetable']['id'],
      )

    @schedules << schedule if schedule.save
  end


end
=end

