class EvaluationReportsController < ApplicationController
  before_action :get_report, only: [:show, :update]
  
  def show
    render 'evaluation_reports/show'
  end
  
  def update
    if @evaluation_report.update(report_params)
      @trip.update(status: 'completed')
      
      render 'evaluation_reports/show'
    else
      render json: @evaluation_report.errors, status: :unprocessable_entity      
    end
  end
  
  private 
  
  def report_params
    params.require(:evaluation_report).permit(
      :pre_trip_prep_rating, 
      :post_trip_prep_rating, 
      :trip_experience_rating, 
      :time_use_rating, 
      :trip_notes, 
      :activity_data_report => [
        :eat_drink, 
        :communicate, 
        :read, 
        :write,
        :browse,
        :video,
        :audio,
        :games,
        :sleep,
        :socialise,
        :window_gaze,
        :care, 
        :other,
      ], 
      :experience_factors_report => [
        :timewise,
        :prep,
        :freedom, 
        :reliability,
        :information,
        :crowding, 
        :seating, 
        :power, 
        :temperature, 
        :noise, 
        :cleanliness,
        :privacy, 
        :security, 
        :passengers,
        :station,
        :other, 
      ]      
    )
  end
  
  def get_report
    @trip = current_user.trips.find(params[:trip_id])
    @evaluation_report = @trip.evaluation_report if @trip
  end
end

