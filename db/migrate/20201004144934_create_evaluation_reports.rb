class CreateEvaluationReports < ActiveRecord::Migration[6.0]
  def change
    create_table :evaluation_reports do |t|
      t.references :trip, null: false, foreign_key: true
      t.integer :pre_trip_prep_rating, default: 0
      t.integer :post_trip_prep_rating, default: 0
      t.integer :trip_experience_rating, default: 0
      t.integer :time_use_rating, default: 0
      t.text :trip_notes

      t.timestamps
    end
  end
end
