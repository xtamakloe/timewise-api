class CreateFeedbackPosts < ActiveRecord::Migration[6.0]
  def change
    create_table :feedback_posts do |t|
      t.references :user, null: false, foreign_key: true
      t.text :message
      t.binary :image

      t.timestamps
    end
  end
end
