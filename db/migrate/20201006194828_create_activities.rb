class CreateActivities < ActiveRecord::Migration[6.0]
  def change
    create_table :activities do |t|
      t.references :activity_data_report, null: false, foreign_key: true
      t.string :code

      t.timestamps
    end
  end
end
