class CreateExperienceFactors < ActiveRecord::Migration[6.0]
  def change
    create_table :experience_factors do |t|
      t.references :experience_factors_report, null: false, foreign_key: true
      t.string :code
      t.integer :rating

      t.timestamps
    end
  end
end
