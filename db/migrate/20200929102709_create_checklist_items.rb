class CreateChecklistItems < ActiveRecord::Migration[6.0]
  def change
    create_table :checklist_items do |t|
      t.references :checklist, null: false, foreign_key: true
      t.text :text
      t.boolean :complete, default: false 

      t.timestamps
    end
  end
end
