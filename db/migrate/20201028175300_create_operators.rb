class CreateOperators < ActiveRecord::Migration[6.0]
  def change
    create_table :operators do |t|
      t.string :code
      t.string :name
      t.string :info_url
      t.string :covid_url
      t.string :crowding_url

      t.timestamps
    end
  end
end
