class CreateActivityDataReports < ActiveRecord::Migration[6.0]
  def change
    create_table :activity_data_reports do |t|
      t.references :evaluation_report, null: false, foreign_key: true
      t.boolean :eat_drink, default: false
      t.boolean :communicate, default: false
      t.boolean :read, default: false
      t.boolean :write, default: false
      t.boolean :browse, default: false
      t.boolean :video, default: false
      t.boolean :audio, default: false
      t.boolean :games, default: false
      t.boolean :sleep, default: false
      t.boolean :socialise, default: false
      t.boolean :window_gaze, default: false
      t.boolean :care, default: false
      t.boolean :other, default: false

      t.timestamps
    end
  end
end
