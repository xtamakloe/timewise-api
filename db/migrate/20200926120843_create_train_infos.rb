class CreateTrainInfos < ActiveRecord::Migration[6.0]
  def change
    create_table :train_infos do |t|
      t.references :trip, null: false, foreign_key: true

      t.timestamps
    end
  end
end
