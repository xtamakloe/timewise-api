class CreateExperienceFactorsReports < ActiveRecord::Migration[6.0]
  def change
    create_table :experience_factors_reports do |t|
      t.references :evaluation_report, null: false, foreign_key: true
      t.integer :timewise, default: 0
      t.integer :prep, default: 0
      t.integer :freedom, default: 0
      t.integer :reliability, default: 0
      t.integer :information, default: 0
      t.integer :crowding, default: 0
      t.integer :seating, default: 0
      t.integer :power, default: 0
      t.integer :temperature, default: 0
      t.integer :noise, default: 0
      t.integer :cleanliness, default: 0
      t.integer :privacy, default: 0
      t.integer :security, default: 0
      t.integer :passengers, default: 0
      t.integer :station, default: 0
      t.integer :other, default: 0

      t.timestamps
    end
  end
end
