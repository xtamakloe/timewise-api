class AddFeatureListToTrainInfos < ActiveRecord::Migration[6.0]
  def change
    add_column :train_infos, :feature_list, :text
  end
end
